import { MongoClient } from 'mongodb';

import nextConnect from 'next-connect';

const client = new MongoClient('mongodb+srv://tmp_antTabs:kgQ0QGuovtoPLGA4@anttabs.oh3cy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

async function database(req: any, res: any, next: any) {
  if (!client.isConnected()) await client.connect();
  req.dbClient = client;
  req.db = client.db('AntTabs');
  return next();
}

const middleware = nextConnect();
middleware.use(database);
export default middleware;