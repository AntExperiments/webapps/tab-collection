import styles from '../styles/Collection.module.scss';
import { useRouter } from 'next/router';

//#region Interfaces

interface link {
    title: string;
    description: string;
    favicon: string;
}

interface Data {
    title: string;
    links: link[];
}

interface Props {
    data: Data;
}

//#endregion

const Collection = ({ data }: Props) => {
    const router = useRouter();
    const { id } = router.query;

    return (
        <div className={styles.card}>
            <span className={styles.title}>{data.title}</span>
            <div className={styles.links}>
                {data.links.map(card => (
                    <div>
                        <img src={card.favicon} alt=""/>
                        <b>{card.title}</b>
                        <span>{card.description}</span>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default Collection;

export async function getServerSideProps() {
  const res = await fetch("http://localhost:3000/api/collection");

  return {
    props: {
      data: await res.json(),
    }
  };
}