import '../styles/globals.css'
import styles from '../styles/Root.module.css'
import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <div className={styles.wrapper}>
      <Component {...pageProps} />
    </div>
  )
}
export default MyApp
